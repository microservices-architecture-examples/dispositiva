package com.kdmforce.dispositiva;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DispositivaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DispositivaApplication.class, args);
	}

}

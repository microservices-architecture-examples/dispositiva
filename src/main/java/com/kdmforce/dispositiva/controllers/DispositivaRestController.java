package com.kdmforce.dispositiva.controllers;

import com.kdmforce.dispositiva.services.DispositivaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
public class DispositivaRestController {


    @Autowired
    private DispositivaService dispositivaService;

    @PostMapping(path="/dispositiva", produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getMessage(@RequestBody String body) {

        return dispositivaService.process(body);
    }
}

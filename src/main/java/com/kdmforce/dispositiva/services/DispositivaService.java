package com.kdmforce.dispositiva.services;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import java.io.IOException;
import java.util.concurrent.TimeoutException;


@Service
public class DispositivaService {
    private static Log logger = LogFactory.getLog(DispositivaService.class);

    @Value("${spring.rabbitmq.queue.name}")
    private String TASK_QUEUE_NAME;

    public ResponseEntity<String> process(String body) {
        logger.debug("message to process: " + body );

        try {
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(body);
            sendMessageDispositiva(jsonObject);
        } catch (ParseException e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (IOException e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
//        ResponseEntity<String>
        return new ResponseEntity(HttpStatus.OK);
    }

    public void sendMessageDispositiva(JSONObject messageToSend) throws IOException {

        JSONObject message = new JSONObject();

        message.put("document", messageToSend);
        message.put("timestamp", System.currentTimeMillis());


        ConnectionFactory factory = new ConnectionFactory();

        Connection connection = null;
        try {
            connection = factory.newConnection();

            Channel channel = connection.createChannel();

            channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);

            channel.basicPublish( "", TASK_QUEUE_NAME, MessageProperties.PERSISTENT_TEXT_PLAIN, message.toJSONString().getBytes());

            channel.close();
        } catch (TimeoutException e) {
            logger.error(e);
        }
        connection.close();
    }



}
